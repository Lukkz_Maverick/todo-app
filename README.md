# ToDo-App

An app that lets you create, read, edit and delete items.
To delete an item swipe left, and to edit it is necessary to press and hold the item. The other interactions are quite intuitive.

